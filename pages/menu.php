<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="<?=$jabatan?>">SB Admin v2.0</a>
    </div>
    <!-- /.navbar-header -->

    <ul class="nav navbar-top-links navbar-right">

        <!-- /.dropdown -->
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-user">
                <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                </li>
                <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                </li>
                <li class="divider"></li>
                <li><a href="../logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                </li>
            </ul>
            <!-- /.dropdown-user -->
        </li>
        <!-- /.dropdown -->
    </ul>
    <!-- /.navbar-top-links -->

    <!-- /.sidebar-collapse -->
    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
                <li class="sidebar-search">
                    <div class="input-group custom-search-form">
                        <input type="text" class="form-control" placeholder="Search...">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button">
                                <i class="fa fa-search"></i>
                            </button>
                        </span>
                    </div>
                    <!-- /input-group -->
                </li>

                <li>
                    <a href="<?=$jabatan?>"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                </li>


                <?php if($jabatan=='lb'){ ?> 
                   <li>
                    <a href="lb_insert"><i class="fa fa-table fa-fw"></i> Tambah</a>
                </li>
                <? } ?>

                  <?php if($jabatan=='lik'){ ?> 
                   <li>
                <a href="#"><i class="fa fa-files-o fa-fw"></i> Input<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="lik_mhs">Tamba Mahasiswa</a>
                    </li>
                    <li>
                        <a href="lik_prodi">Tamba Program Studi</a>
                    </li>
                    <li>
                        <a href="lik_fakultas">Tamba Fakultas</a>
                    </li>
                </ul>
                <? } ?>

                <?php if($jabatan=='lkmk'){ ?> 
                   <li>
                    <a href="lkmk_insert"><i class="fa fa-table fa-fw"></i> Tambah</a>
                </li>
                <li>
                    <a href="forms.html"><i class="fa fa-edit fa-fw"></i> Forms</a>
                </li>
                <? } ?>

            <!-- <li>
                <a href="#"><i class="fa fa-files-o fa-fw"></i> Sample Pages<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="blank.html">Blank Page</a>
                    </li>
                    <li>
                        <a href="login.html">Login Page</a>
                    </li>
                </ul>
            </li> -->
        </ul>
    </div>
</div> 
</nav>