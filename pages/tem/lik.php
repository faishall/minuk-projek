<?include DIV_CLASS.'/lik'.CLASS_; ?>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Mahasiswa
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>NIM</th>
                                            <th>Nama</th>
                                            <th>Vakultas</th>
                                            <th>Prodi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?$no=0; while($data = mysqli_fetch_assoc($lik)){ $no++?>
                                        <tr>
                                              <td><?=$no?></td>
                                              <td><?=$data['nim']?></td>
                                              <td><?=$data['nama']?></td>
                                              <td><?=$data['nama_fakultas']?></td>
                                              <td><?=$data['nama_prodi']?></td>
                                        </tr>
                                        <?}?>
                                        
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-6 -->
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Program Studi
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Program studi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <? $no=0; while($data = mysqli_fetch_assoc($prodi)){ $no++?>
                                        <tr>
                                            <td><?=$no?></td>
                                            <td><?=$data['nama_prodi']?></td>
                                        </tr>
                                        <?}?>
                                        
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-6 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Fakultas
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Fakultas</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <? $no=0; while($data = mysqli_fetch_assoc($fakultas)){ $no++?>
                                        <tr>
                                            <td><?=$no?></td>
                                            <td><?=$data['nama_fakultas']?></td>
                                        </tr>
                                        <?}?>
                                       
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-6 -->
               
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-6 -->
            </div>
            <!-- /.row -->
        </div>