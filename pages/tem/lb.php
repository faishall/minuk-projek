<!DOCTYPE html>
<?include DIV_CLASS.'/lb'.CLASS_; ?>
<html>
<head>
<style>
#customers {
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

#customers td, #customers th {
    border: 1px solid #ddd;
    padding: 8px;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}

#customers th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: left;
    background-color: #6d716d;
    color: white;
    text-align: center;
}
#td {
    text-align: center;
}
</style>
</head>
<body>

<table id="customers">
 <tr>
    <th rowspan="2">NO</th>
    <th rowspan="2">NIM</th>
    <th rowspan="2">NAMA</th>
    <th rowspan="2">FAKULTAS</th>
    <th rowspan="2">PRODI</th>
    <th rowspan="2">MATAKULIAH</th>
    <th rowspan="2">NILAI</th>
    <th rowspan="2">NILAI BACA</th>
  </tr>
  <tr>
    <!-- <th colspan="2"><center>B.INGGRIS</center></th>
    <th colspan="2"><center>B.ARAB</center></th>
    <th colspan="2"><center>B.MANDARAIN</center></th> -->
  </tr>
  <?
  while($data = mysqli_fetch_assoc($lb)){ $no++?>

  <tr>
      <td id="td"><?=$no?></td>
      <td id="td"><?=$data['nim']?></td>
      <td><?=$data['nama']?></td>
      <td><?=$data['nama_fakultas']?></td>
      <td><?=$data['nama_prodi']?></td> 
      <td><?=$data['matkul']?></td>
      <td id="td"><?=$data['nilai']?></td>
      <td><?=$data['nilai_baca']?></td>
  </tr>
  <?}?>
  
</table>

</body>
</html>