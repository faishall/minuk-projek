-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 24 Sep 2018 pada 19.08
-- Versi server: 10.1.35-MariaDB
-- Versi PHP: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `new`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `login`
--

CREATE TABLE `login` (
  `user_id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `jabatan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `login`
--

INSERT INTO `login` (`user_id`, `username`, `password`, `jabatan`) VALUES
(0, 'lkmk', 'admin', 'lkmk'),
(2, 'lik', 'admin', 'lik'),
(3, 'lb', 'admin', 'lb');

-- --------------------------------------------------------

--
-- Struktur dari tabel `_akademik`
--

CREATE TABLE `_akademik` (
  `id_akademik` int(11) NOT NULL,
  `nim` varchar(11) NOT NULL,
  `id_matkul` varchar(11) NOT NULL,
  `nilai` varchar(11) NOT NULL,
  `nilai_baca` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `_dosen`
--

CREATE TABLE `_dosen` (
  `id_dosen` int(11) NOT NULL,
  `nama_dosen` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `tlp` varchar(13) NOT NULL,
  `id_matkul` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `_fakultas`
--

CREATE TABLE `_fakultas` (
  `id_fakultas` int(11) NOT NULL,
  `nama_fakultas` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `_fakultas`
--

INSERT INTO `_fakultas` (`id_fakultas`, `nama_fakultas`) VALUES
(1, 'Teknik Informatika'),
(2, 'Teknik Nuklir'),
(3, 'faihsall'),
(5, 'Dakwa');

-- --------------------------------------------------------

--
-- Struktur dari tabel `_mahasiswa`
--

CREATE TABLE `_mahasiswa` (
  `id_mhs` int(11) NOT NULL,
  `nim` varchar(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `id_fakultas` int(11) NOT NULL,
  `id_prodi` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `_mahasiswa`
--

INSERT INTO `_mahasiswa` (`id_mhs`, `nim`, `nama`, `id_fakultas`, `id_prodi`) VALUES
(5, '123456', 'MIkuk Ruli', 4, 4),
(6, '12347', 'fadli yanto', 1, 3),
(7, '123444', 'Intan', 2, 3),
(8, '12344', 'Intan', 2, 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `_matkul`
--

CREATE TABLE `_matkul` (
  `id_matkul` int(11) NOT NULL,
  `matkul` varchar(100) NOT NULL,
  `lembaga` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `_matkul`
--

INSERT INTO `_matkul` (`id_matkul`, `matkul`, `lembaga`) VALUES
(1, 'Bahasa Arab', ''),
(2, 'Bahasa Inggris', ''),
(3, 'Bahasa Mandarin', ''),
(5, 'Furudul Ainiyah', ''),
(6, 'Baca Tulis Al-Quran', ''),
(7, 'Aswaja', ''),
(8, 'Kepesantrenan', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `_pengguna`
--

CREATE TABLE `_pengguna` (
  `id_pengguna` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `jabatan` varchar(100) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `_prodi`
--

CREATE TABLE `_prodi` (
  `id_prodi` int(11) NOT NULL,
  `nama_prodi` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `_prodi`
--

INSERT INTO `_prodi` (`id_prodi`, `nama_prodi`) VALUES
(3, 'Teknik'),
(4, 'Filsafat'),
(5, 'Ilmu Hadis');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`user_id`);

--
-- Indeks untuk tabel `_akademik`
--
ALTER TABLE `_akademik`
  ADD PRIMARY KEY (`id_akademik`);

--
-- Indeks untuk tabel `_dosen`
--
ALTER TABLE `_dosen`
  ADD PRIMARY KEY (`id_dosen`);

--
-- Indeks untuk tabel `_fakultas`
--
ALTER TABLE `_fakultas`
  ADD PRIMARY KEY (`id_fakultas`);

--
-- Indeks untuk tabel `_mahasiswa`
--
ALTER TABLE `_mahasiswa`
  ADD PRIMARY KEY (`id_mhs`);

--
-- Indeks untuk tabel `_matkul`
--
ALTER TABLE `_matkul`
  ADD PRIMARY KEY (`id_matkul`);

--
-- Indeks untuk tabel `_pengguna`
--
ALTER TABLE `_pengguna`
  ADD PRIMARY KEY (`id_pengguna`);

--
-- Indeks untuk tabel `_prodi`
--
ALTER TABLE `_prodi`
  ADD PRIMARY KEY (`id_prodi`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `_akademik`
--
ALTER TABLE `_akademik`
  MODIFY `id_akademik` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT untuk tabel `_dosen`
--
ALTER TABLE `_dosen`
  MODIFY `id_dosen` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `_fakultas`
--
ALTER TABLE `_fakultas`
  MODIFY `id_fakultas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `_mahasiswa`
--
ALTER TABLE `_mahasiswa`
  MODIFY `id_mhs` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `_matkul`
--
ALTER TABLE `_matkul`
  MODIFY `id_matkul` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `_pengguna`
--
ALTER TABLE `_pengguna`
  MODIFY `id_pengguna` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `_prodi`
--
ALTER TABLE `_prodi`
  MODIFY `id_prodi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
